<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handlers.
 *
 * @package    local_coventrycodes
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_coventrycodes;
defined('MOODLE_INTERNAL') || die();

/**
 * Handlers.
 *
 * @package    local_coventrycodes
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class handlers {

    /**
     * Display completion progress bar.
     *
     * @param string $shortcode The shortcode.
     * @param array $args The arguments.
     * @param null $content The content.
     * @param stdClass $env The environment.
     * @param Closure $next The next callback.
     * @return string
     */
    public static function completion_progress($shortcode, $args, $content, $env, $next) {
        global $PAGE, $USER;

        // Non-logged in and guests don't see anything.
        if (!isloggedin() || isguestuser($USER)) {
            return '';
        }

        $renderer = $PAGE->get_renderer('local_coventrycodes');
        $sectionid = $args['sectionid'] ?? null;
        $courseid = $args['courseid'] ?? null;

        // Infer the course ID if none were provided.
        if (empty($sectionid) && empty($courseid)) {
            $coursecontext = $env->context->get_course_context(false);

            // If still empty, then we abandon.
            if (empty($coursecontext)) {
                return $renderer->error_notice($shortcode, get_string('notenoughargumentstodisplay', 'local_coventrycodes'));
            }

            $courseid = $coursecontext->instanceid;
        }

        try {
            $progresscalculator = new progress_calculator($USER->id);
            if (!empty($sectionid)) {
                $progress = $progresscalculator->get_section_progress($sectionid);
            } else {
                $progress = $progresscalculator->get_course_progress($courseid);
            }
        } catch (\Exception $e) {
            return $renderer->error_notice($shortcode, $e->getMessage());
        }

        return $renderer->progress_bar($progress);
    }

}
