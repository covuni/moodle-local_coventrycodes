<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer.
 *
 * @package    local_coventrycodes
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_coventrycodes\output;
defined('MOODLE_INTERNAL') || die();

use html_writer;

/**
 * Renderer.
 *
 * @package    local_coventrycodes
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class renderer extends \plugin_renderer_base {

    /**
     * Display an error notice.
     *
     * @param string $shortcode The shortcode.
     * @param string $message The error message.
     * @return string
     */
    public function error_notice($shortcode, $message) {
        global $PAGE;
        if (!has_capability('local/coventrycodes:viewnotices', $PAGE->context)) {
            return '';
        }
        return html_writer::tag('em', get_string('debugnotice', 'local_coventrycodes', (object) [
            'shortcode' => $shortcode,
            'message' => $message
        ]));
    }

    /**
     * Renders a progress bar.
     *
     * @param float $progress The progress ratio (between 1 and 0)
     * @return string
     */
    public function progress_bar($progress) {
        return $this->render_from_template('block_myoverview/progress-bar', (object) [
            'progress' => floor($progress * 100)
        ]);
    }

}
