<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Completion calculator.
 *
 * @package    local_coventrycodes
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_coventrycodes;
defined('MOODLE_INTERNAL') || die();

use moodle_exception;

/**
 * Progress calculator.
 *
 * This computes the completion progress of a user, not based on the activities that
 * are a criteria for the course completion, but based on the number of activities
 * with completion enabled.
 *
 * @package    local_coventrycodes
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class progress_calculator {

    /**
     * Constructor.
     *
     * @param int $userid The user ID.
     */
    public function __construct($userid) {
        $this->userid = $userid;
    }

    /**
     * Compute the progress based on a list of course modules.
     *
     * @param array $cms Array of cm_info.
     * @return float
     */
    protected function compute_progress($cms) {
        global $DB;

        $total = count($cms);
        if ($total <= 0) {
            return 1;
        }

        $cmids = array_map(function($cm) {
            return $cm->id;
        }, $cms);
        list($insql, $inparams) = $DB->get_in_or_equal($cmids, SQL_PARAMS_NAMED);

        $sql = "userid = :userid AND coursemoduleid $insql AND completionstate IN (:completed, :pass, :fail)";
        $params = [
            'userid' => $this->userid,
            'completed' => COMPLETION_COMPLETE,
            'pass' => COMPLETION_COMPLETE_PASS,
            'fail' => COMPLETION_COMPLETE_FAIL,
        ] + $inparams;
        $completed = $DB->count_records_select('course_modules_completion', $sql, $params);
        return $completed / $total;
    }

    /**
     * Get a list of course modules.
     *
     * @param int $courseid The course ID.
     * @param int $sectionnum The section number.
     * @return array
     */
    protected function get_cms($courseid, $sectionnum = null) {
        $modinfo = get_fast_modinfo($courseid, $this->userid);

        if ($sectionnum !== null) {
            $cmids = $modinfo->sections[$sectionnum] ?? [];
            $cms = array_map(function($cmid) use ($modinfo) {
                return $modinfo->get_cm($cmid);
            }, $cmids);
        } else {
            $cms = $modinfo->get_cms();
        }

        $this->require_completionlib();
        return array_filter($cms, function($cm) {
            return $cm->completion != COMPLETION_TRACKING_NONE;
        });
    }

    /**
     * Get the course progress value.
     *
     * @param int $courseid The course ID.
     * @return float Between 0 and 1.
     */
    public function get_course_progress($courseid) {
        $cms = $this->get_cms($courseid);
        return $this->compute_progress($cms);
    }

    /**
     * Get the section progress value.
     *
     * @param int $sectionid The section ID.
     * @return float Between 0 and 1.
     */
    public function get_section_progress($sectionid) {
        global $DB;

        // Resolve the section.
        $sectiondetails = $DB->get_record('course_sections', ['id' => $sectionid], 'id, course, section', IGNORE_MISSING);
        if (empty($sectiondetails)) {
            throw new moodle_exception('invalidsectionid', 'local_coventrycodes');
        }

        $sectionnum = $sectiondetails->section;
        $courseid = $sectiondetails->course;

        $cms = $this->get_cms($courseid, $sectionnum);
        return $this->compute_progress($cms);
    }

    /**
     * Require the completion lib.
     *
     * @return void
     */
    protected function require_completionlib() {
        global $CFG;
        require_once($CFG->libdir . '/completionlib.php');
    }

}
