<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package    local_coventrycodes
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['coventrycodes:viewnotices'] = 'View shortcodes debugging notices.';
$string['debugnotice'] = 'The shortcode \'{$a->shortcode}\' encountered the following issue: {$a->message}.';
$string['invalidsectionid'] = 'Invalid section ID';
$string['notenoughargumentstodisplay'] = 'Arguments were missing or invalid for the shortcode to display.';
$string['pluginname'] = 'Coventry Shortcodes';
$string['shortcodecompletionprogress'] = 'Displays the completion progress of a course or section';
$string['shortcodecompletionprogress_help'] = '
This shortcode displays a progress bar representing the percentage of activities completed by the current user in a given course or section.

It supports the following arguments:

- `courseid` (optional) The database ID of the course to display the progress of. Defaults to the current course, if any. The value is ignored when `sectionid` is set.
- `sectionid` (optional) The database ID of the section to display the progress of. This infers the `courseid`.

The shortcode will not display anything when:

- The current user is guest or not logged in
- The given resources (course or section) does not exist or could not be inferred

Note that users with the permission to `local/coventryscode:viewnotices` will see debugging messages when shortcodes encounteres an issue.
';
