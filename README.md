Coventry's Shortcodes
=====================

This plugins holds a collection of `shortcodes` for Coventry's use.

Dependencies
------------

- Moodle 3.6
- Plugin filter_shortcodes

Installation
------------

1. Extract the content of the plugin in `local/coventrycodes`.
2. Navigate to _Site administration > Notifications_ to trigger the upgrade.
3. Ensure that the filter plugin _Shortcodes_ is enabled in the contexts in which shortcodes will be used.

Usage
-----

Simply add the various shortcodes in the content where it should be displayed.

For a list of available shortcodes, and how to use them, visit the page provided by the _Shortcodes_ plugin at:

> moodle.example.com/filter/shortcodes

Notes
-----

Using specific IDs in content that will be backed up and restored will not work as the restore process does not allow for these IDs to be updated.

Users with the capability `local/coventrycodes:viewnotices` may be shown notices when shortcodes encounter an issue.
